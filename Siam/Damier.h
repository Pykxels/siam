#ifndef DAMIER_H
#define DAMIER_H
#include "pion.h"
#include <vector>

using namespace std;

class Damier
{
    public:
        Damier();
        ~Damier();
        void SetTheTab(vector<vector<Pion*> > p_tab);   /// m_tab prend la valeur p_tab
        void SetPion(Pion* _tmp, int i, int j);  ///m_tab aux coordonn�es i,j prend la valeur _tmp
        void SetResE(bool signe);   ///Fonction qui ajoute ou enleve un elephant � la reserve selon le signe
        void SetResR(bool signe);   ///Fonction qui ajoute ou enleve un rhino � la reserve selon le signe
        int GetSizeE ();   ///R�cup�ration de la taille de la reserve d'elephants
        int GetSizeR ();   ///R�cup�ration de la taille de la reserve de rhinos
        Pion* GetPion(int i, int j);
        vector<Pion*> GetResE (); ///On recupere la reserve d'elephants
        vector<Pion*> GetResR (); ///On recupere la reserve de rhinos
    private:
        vector<vector<Pion*> > m_tab;
        vector <Pion*> m_resE;
        vector <Pion*> m_resR;
};

#endif // DAMIER_H
