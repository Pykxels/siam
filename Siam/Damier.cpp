#include "Damier.h"
#include "Animal.h"
#include "Montagne.h"
#include <vector>
#include <cstddef>

Damier::Damier()
{

    Pion * olo=NULL;
    vector<Pion*> ere;
    for(int i = 0; i<5; i++)
    {
        for(int j =0; j<5; j++)
        {
            ere.push_back(olo);
        }
        m_tab.push_back(ere);
        ere.clear();
    }
    int x=3;
    for (int y=2; y<5; y++)
    {
        Montagne* tmp = new Montagne();
        m_tab[x][y]=tmp;
    }
    for(int i=0; i<5; i++)   ///   0 on enl�ve 1 on ajoute
    {
        Animal* aniE=new Animal(2);  ///On cr�e un �l�phant
        m_resE.push_back(aniE);
        Animal* aniR=new Animal(3);  ///On cr�e un �l�phant
        m_resR.push_back(aniR);
    }

}

Damier::~Damier()
{
    //dtor
}

void Damier::SetTheTab(vector<vector<Pion*> > p_tab)
{
    m_tab=p_tab;
}

void Damier::SetPion(Pion* _tmp, int i, int j)
{
    m_tab[i][j]=_tmp;
}

void Damier::SetResE(bool signe)
{
    switch(signe)
    {
    case 0:
        {
            Animal* ani=new Animal(2);  ///On cr�e un �l�phant
            m_resE.push_back(ani);
            break;
        }
    case 1:
        {
            m_resE.pop_back();
            break;
        }
    }

}
void Damier::SetResR(bool signe)
{
    switch(signe)
    {
    case false:
        {
            Animal* ani=new Animal(3);  ///On cr�e un �l�phant
            m_resR.push_back(ani);
            break;
        }
    case true:
        {
            m_resR.pop_back();
            break;
        }
    }

}

Pion* Damier::GetPion(int i, int j)
{
    return m_tab[i][j];
}

int Damier::GetSizeE ()
{
    return m_resE.size();
}

int Damier::GetSizeR ()
{
    return m_resR.size();
}

vector<Pion*> Damier::GetResE ()
{
    return m_resE;
}

vector<Pion*> Damier::GetResR ()
{
    return m_resR;
}
