#include "console.h"
#include <conio.h>
#include <windows.h>
#include"Pion.h"


Console* Console::m_instance = NULL;

Console::Console()
{
    m_instance = NULL;
}

Console::~Console()
{

}

void Console::deleteInstance()
{
    delete m_instance;
    m_instance = NULL;
}

Console* Console::getInstance()
{
    if (!Console::m_instance)
    {
        m_instance = new Console();
    }

    return m_instance;
}

void Console::gotoLigCol(int lig, int col)
{
    COORD mycoord;
    mycoord.X = col;
    mycoord.Y = lig;
    SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), mycoord );
}

int Console::getInputKey()
{
    return getch();
}

bool Console::isKeyboardPressed()
{
    return kbhit();
}

/*
0: noir
1: bleu fonc�
2: vert
3: bleu-gris
4: marron
5: pourpre
6: kaki
7: gris clair
8: gris
9: bleu
10: vert fluo
11: turquoise
12: rouge
13: rose fluo
14: jaune fluo
15: blanc
*/
void Console::_setColor(int back, int front)
{
    HANDLE H=GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(H,front*16+back);
}

void Console::setColor(Color col)
{
    switch (col)
    {
    case COLOR_WHITE:
        this->_setColor(15, 0);
        break;
    case COLOR_BLACK:
        this->_setColor(0, 0);
        break;
    case COLOR_RED:
        this->_setColor(12, 0);
        break;
    case COLOR_GREEN:
        this->_setColor(10, 0);
        break;
    case COLOR_BLUE:
        this->_setColor(9, 0);
        break;
    case COLOR_YELLOW:
        this->_setColor(14, 0);
        break;
    case COLOR_PURPLE:
        this->_setColor(5, 0);
        break;
    default:
        this->_setColor(7, 0);
    }
}

void curseur(int interne, int *x, int *y)   ///Reception de &x &y et de interne
{
    Console* pConsole = NULL;

    int lig=2;
    int col=5;
    pConsole->gotoLigCol(lig,col);
    int key=0;



    do
    {


        ///si on appuye sur une touche
        if(pConsole->isKeyboardPressed())
        {
            ///on lit la valeur de la touche
            key=pConsole->getInputKey();

            if (interne==3)
            {
                if(key==72)
                {
                    if(lig>2)
                    {
                        lig=lig-2;
                        pConsole->gotoLigCol(lig,col);
                    }
                }

                if(key==80)
                {
                    if(lig<10)
                    {
                        lig=lig+2;
                        pConsole->gotoLigCol(lig,col);
                    }
                }

                if(key==77)
                {
                    if(col<20)
                    {
                        col=col+4;
                        pConsole->gotoLigCol(lig,col);
                    }
                }
                if(key==75)
                {
                    if(col>8)
                    {
                        col=col-4;
                        pConsole->gotoLigCol(lig,col);
                    }
                }



            }
            if ((interne==1)||(interne==2))
            {
                if(key==72)
                {
                    if(((col==5)&&(lig==4))||((col==5)&&(lig==6))||((col==5)&&(lig==8))
                            ||((col==5)&&(lig==10))||((col==21)&&(lig==4))
                            ||((col==21)&&(lig==6))||((col==21)&&(lig==8))||((col==21)&&(lig==10)))
                    {
                        lig=lig-2;
                        pConsole->gotoLigCol(lig,col);
                    }
                }

                if  (key==80)
                {
                    if (((col==5)&&(lig==2))||((col==5)&&(lig==4))||((col==5)&&(lig==6))||((col==5)&&(lig==8))
                            ||((col==21)&&(lig==2))||((col==21)&&(lig==4))
                            ||((col==21)&&(lig==6))||((col==21)&&(lig==8)))
                    {
                        lig=lig+2;
                        pConsole->gotoLigCol(lig,col);
                    }
                }

                if(key==77)
                {
                    if(((col==5)&&(lig==2))||((col==9)&&(lig==2))||((col==13)&&(lig==2))||((col==17)&&(lig==2))
                            ||((col==5)&&(lig==10))||((col==9)&&(lig==10))
                            ||((col==13)&&(lig==10))||((col==17)&&(lig==10)))
                    {
                        col=col+4;
                        pConsole->gotoLigCol(lig,col);
                    }
                }
                if(key==75)
                {
                    if(((col==9)&&(lig==2))||((col==13)&&(lig==2))||((col==17)&&(lig==2))
                            ||((col==21)&&(lig==2))||((col==9)&&(lig==10))
                            ||((col==13)&&(lig==10))||((col==17)&&(lig==10))||((col==21)&&(lig==10)))
                    {
                        col=col-4;
                        pConsole->gotoLigCol(lig,col);
                    }
                }

                /* if(key==13) ///touche entr�e
                {
                    *x=(col/4)-(5/4);
                    *y=(lig/2)-1;
                    return 0;
                }
                */
            }

        }
    }
    while(key!=13);  ///tant qu'on appuye pas sur entr�e


    *x=(col/4)-(5/4);
    *y=(lig/2)-1;

}
