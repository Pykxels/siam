#include <iostream>
#include <cstddef>
#include "damier.h"
#include "Pion.h"
#include "Montagne.h"
#include <vector>
#include"affichage.h"
#include <stdlib.h>
#include<stdio.h>
#include "windows.h"
#include <conio.h>
#include "Animal.h"
#include "Console.h"
#include "menus.h"

using namespace std;

void insertion(Damier* dam, bool* T, int x, int y);

void suppression(Damier* dam, bool* T, int x, int y);

Damier deplacement(Damier dam, bool* TOUR);

void direction(Damier* dam, int x, int y);

void bouclejeu()
{
    Console* pConsole=NULL;
    pConsole=Console::getInstance();
    char C='W';
    Damier dam;
    affichage(dam);

    int x, y;
    int V=0;
    bool* T=new bool;
    bool Ty;  ///false: tour de l'elephant  true: tour du rhino
    bool* T1=new bool;
    *T=0;
    char choice;
    int interne;
    while (V==0)
    {

        *T1=*T;

        while(*T==*T1)
        {
            affichage(dam);
            pConsole->gotoLigCol(13,0);

            cout << "A.Inserer un pion" << endl;
            cout << "Z.Enlever un pion" << endl;
            cout << "E.Deplacer un pion" << endl;
            cout << "R.Orienter un pion" << endl;
            cout << "T.Quitter" << endl;

            while(C!='A'&&C!='Z'&&C!='E'&&C!='R'&&C!='T')
            {
                C=getch();
            }

            switch(C)
            {
            case 'A':  ///Choix 1: Faire rentrer un pion
            {
                interne=1;
                curseur(interne, &x, &y);
                insertion(&dam, T, x, y);
                break;
            }
            case 'Z':  ///Choix 2: Enlever un pion du plateau
            {
                interne=2;
                curseur(1, &x, &y);
                suppression(&dam, T, x, y);
                break;
            }
            case 'E':  ///Choix 3: Deplacer un pion sur le plateau
            {
                interne=3;
                deplacement(dam, T);
                break;
            }
            case 'R': ///Choix 4: Deplacer
            {
                interne=3;

                break;
            }
            case 'T':
            {
                quitter();
                break;
            }
            }

        }
    }

}

void insertion(Damier* dam, bool* T, int x, int y)
{
    Pion* tmp;
    tmp=dam->GetPion(x,y);
    if(tmp==NULL)
    {

    //if(((tmp->GetType()==2)&&(*T==false))||((tmp->GetType()==3)&&(*T==true)))
    //{

        switch(*T)
        {
        case false:
        {
            Animal* ani=new Animal(2);  ///je cr�er l'elephant
            dam->SetResE(1);   ///je retire une case � la reserve ele
            dam->SetPion(ani, x, y); ///Je pose le pion sur le damier aux coordonn�es x y
            direction(dam, x, y);
            *T=true;

            break;
        }
        case true:
        {
            Animal* ani=new Animal(3);  ///je cr�er le rhino
            dam->SetResR(1);  ///je retire une case � la reserve rhino
            dam->SetPion(ani, x, y); ///Je pose le pion sur le damier aux coordonn�es x y
            direction(dam, x, y);
            *T=false;

            break;
        }
        //}
        }}
}

void suppression(Damier* dam, bool* T, int x, int y)
{
    switch(*T)
    {
    case false:
    {
        dam->SetResE(0);   ///je retire une case � la reserve ele
        dam->SetPion(NULL, x, y); ///Je pose le pion sur le damier aux coordonn�es x y
        break;
    }
    case true:
    {
        dam->SetResR(0);  ///je retire une case � la reserve rhino
        dam->SetPion(NULL, x, y); ///Je pose le pion sur le damier aux coordonn�es x y
        break;
    }
    }
}

Damier deplacement(Damier dam, bool* TOUR)
{
    int x;
    int y;
    int x1;
    int y1;
    char choix='O';
    curseur(1, &x, &y);
    Console* pConsole=NULL;
    pConsole=Console::getInstance();
    ///Pointeur qui choisit une case
    if(*TOUR==false)
    {
        if(dam.GetPion(x,y)->GetType()!=2)
        {
            return dam;
        }
    }
    if(*TOUR==true)
    {
        if(dam.GetPion(x,y)->GetType()!=3)
        {
            return dam;
        }
    }
    ///Analyse du choix
    system("cls");
    affichage(dam);
    pConsole->gotoLigCol(13,0);

    cout << "Vers ou voulez vous aller?" << endl << "H: haut    B: bas" << endl << "D: droite   G: gauche" << endl;
    while(choix!='H'||choix!='B'||choix!='D'||choix!='G')
    {
        choix=getch();
    }
    switch(choix)
    {
    case 'H':
    {
        x1=x;
        y1=y+1;
        break;
    }
    case 'B':
    {
        x1=x;
        y1=y-1;
        break;
    }
    case 'D':
    {
        x1=x+1;
        y1=y;
        break;
    }
    case 'G':
    {
        x1=x-1;
        y1=y;
        break;
    }
    }
    if(0>x1>4&&0>y1>4)
    {
        return dam;

    }
    if(dam.GetPion(x1,y1)!=NULL)
    {
        return dam;
    }
    else if(dam.GetPion(x1,y1)==NULL)
    {
        suppression( &dam, TOUR, x, y);
        insertion( &dam, TOUR, x1, y1);
        *TOUR++;
    }
}

void direction(Damier* dam, int x, int y)
{
    system("cls");
    affichage(*dam);
    Console* pConsole=NULL;
    pConsole=Console::getInstance();
    pConsole->gotoLigCol(13,0);
    char choice;

    cout<< "Quelle est la nouvelle direction de votre pion?" <<endl<< "H: haut  B: bas  D: droite   G: gauche" <<endl;
    while(choice!='H'||choice!='B'||choice!='D'||choice!='G')
    {
        choice=getch();
    }
    Pion* tmp;
    tmp=dam->GetPion(x,y);
    switch(choice)
    {
    case ('H'):
        {
            tmp->SetType('h');
            break;
        }
    case ('B'):
        {
            tmp->SetType('b');
            break;
        }
    case ('D'):
        {
            tmp->SetType('d');
            break;
        }
    case ('G'):
        {
            tmp->SetType('g');
            break;
        }
    }
    dam->SetPion(tmp,x,y);

}

/*void Pousser(bool* TOUR, int x, int y, Damier dam)
{
    float res;
    vector< Pion* > pousse;
    int x1=0;
    int y1=0;
    char a= dam.GetPion(x,y)->Getdir();
    switch(a)
    {
        case ('h'):
        {
            tmp->SetType('h');
            break;
        }
    case ('b'):
        {
            tmp->SetType('b');
            break;
        }
    case ('d'):
        {
            tmp->SetType('d');
            break;
        }
    case ('g'):
        {
            tmp->SetType('g');
            break;
        }
    }


}*/
