#ifndef PION_H
#define PION_H


class Pion
{
    public:
        Pion();
        Pion(int p_type);
        virtual ~Pion();
        int GetType();   ///On r�cup�re le type du Pion
        void SetType(int _type);
    private:
        int m_type;  /// 1: montagne 2:�l�phant 3:rhinoc�ros
        int m_x;
        int m_y;


};

#endif // PION_H
