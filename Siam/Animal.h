#ifndef ANIMAL_H
#define ANIMAL_H
#include "pion.h"

using namespace std;

class Animal : public Pion
{
    public:
        Animal(int p_type);
        virtual ~Animal();
        void SetType(char p_direction);
        char GetDir();
    private:
        char m_direction;
};

#endif // ANIMAL_H
