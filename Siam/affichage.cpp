#include <iostream>
#include "Console.h"
#include "Damier.h"
#include <vector>
#include<stdlib.h>

using namespace std;

void afficherdamier ()
{
    Console* pConsole = NULL;

    pConsole = Console::getInstance();

    int lig=1;
    pConsole->gotoLigCol(lig,3);
    cout << char (201) << char(205)<< char(205) << char (205) << char (203) << char(205) << char(205)<< char (205) << char (203)<< char(205)  << char(205)<< char (205) << char (203)<< char(205)
         << char (205)<< char(205) << char (203) << char (205) << char (205) << char(205)<< char(187)  << endl;
    lig=lig+1;
    for(int i=0; i<4; i++)
    {
        pConsole->gotoLigCol(lig,3);
        cout << char (186) << "   " << char (186) << "   " << char (186) << "   "<< char (186) << "   "<< char (186) << "   " << char (186) << "   "<< endl;
        lig=lig+1;
        pConsole->gotoLigCol(lig,3);
        cout << char (204) << char (205)<< char (205) << char (205) << char (206) << char (205) << char (205)<< char (205) << char (206) << char (205)<< char (205) << char (205)<< char (206) << char (205)<< char (205)
             << char (205) << char (206)<< char (205) << char (205) << char (205) << char(185) << endl;
        lig=lig+1;
        pConsole->gotoLigCol(lig,3);
        cout << char (186) << "   " << char (186) << "   " << char (186) << "   "<< char (186) << "   "<< char (186) << "   " << char (186) << "   "<< endl;
    }

    pConsole->gotoLigCol(lig+1,3);
    cout << char (200) << char (205) << char (205) << char (205) << char (202) << char (205) << char (205)<< char (205) << char (202) << char (205) << char (205)<< char (205) << char (202) << char (205) << char (205)<< char (205) << char (202) << char (205)
         << char (205) << char (205) << char (188) << endl;


    pConsole->gotoLigCol(2,30);
    cout  << "Joueur 1" << endl;
    pConsole->gotoLigCol(3,30);
    cout << char (201) << char(205)<< char(205) << char (205) << char (203) << char(205) << char(205)<< char (205) << char (203)<< char(205)  << char(205)<< char (205) << char (203)<< char(205)
         << char (205)<< char(205) << char (203) << char (205) << char (205) << char(205)<< char(187)  << endl;
    pConsole->gotoLigCol(4,30);
    cout << char (186) << "   " << char (186) << "   " << char (186) << "   "<< char (186) << "   "<< char (186) << "   " << char (186) << "   "<< endl;
    pConsole->gotoLigCol(5,30);
    cout << char (200) << char (205) << char (205) << char (205) << char (202) << char (205) << char (205)<< char (205) << char (202) << char (205) << char (205)<< char (205) << char (202) << char (205) << char (205)<< char (205) << char (202) << char (205)
         << char (205) << char (205) << char (188) << endl;


    pConsole->gotoLigCol(6,30);
    cout  << "Joueur 2" << endl;
    pConsole->gotoLigCol(7,30);
    cout << char (201) << char(205)<< char(205) << char (205) << char (203) << char(205) << char(205)<< char (205) << char (203)<< char(205)  << char(205)<< char (205) << char (203)<< char(205)
         << char (205)<< char(205) << char (203) << char (205) << char (205) << char(205)<< char(187)  << endl;
    pConsole->gotoLigCol(8,30);
    cout << char (186) << "   " << char (186) << "   " << char (186) << "   "<< char (186) << "   "<< char (186) << "   " << char (186) << "   "<< endl;
    pConsole->gotoLigCol(9,30);
    cout << char (200) << char (205) << char (205) << char (205) << char (202) << char (205) << char (205)<< char (205) << char (202) << char (205) << char (205)<< char (205) << char (202) << char (205) << char (205)<< char (205) << char (202) << char (205)
         << char (205) << char (205) << char (188) << endl;
}

void affichervector(Damier dam)
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    int lig=0;
    int col=5;
    int j1L=4;
    int j1C=32;
    int j2L=8;
    int j2C=32;
    int interne=0;
    Pion* tmp;
    int tailleres;
    vector<Pion*> reserve;

    for (int i=0; i<5; i++)
    {
        col=1;
        for (int j=0; j<5; j++)
        {
            pConsole->gotoLigCol(lig,col);
            tmp=dam.GetPion(j,i);

            if (tmp==NULL)
            {
            }
            else if(tmp!=NULL)
        {
            switch(tmp->GetType())
                {
                case 1:
                {
                    cout << "M"<< endl;
                    break;
                }
                case 2:
                {
                    cout << "E" << endl;
                    break;
                }
                case 3:
                {
                    cout << "R" << endl;
                    break;
                }
                }

            }
            col=col+4;
        }
        lig=lig+2;
    }

    tailleres=dam.GetSizeE();
    for (int k=0; k<tailleres; k++)
    {
        pConsole->gotoLigCol(j1L,j1C);
        cout << "E" << endl;
        j1C=j1C+4;

    }
    tailleres=dam.GetSizeR();
    for (int l=0; l<tailleres; l++)
    {
        pConsole->gotoLigCol(j2L,j2C);
        cout << "R" << endl;
        j2C=j2C+4;

    }


}

void affichage(Damier dam)
{
    system("cls");
    afficherdamier();
    affichervector(dam);
}
